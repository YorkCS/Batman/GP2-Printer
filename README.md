# GP2 Printer

## Prereqs

Any modern Linux or Mac running Docker 2018 CE or greater will suffice.

## Usage

The image will attempt to run your Makefile. Simply run:

```bash
$ docker run -v ${PWD}:/data registry.gitlab.com/yorkcs/batman/gp2-printer
```

A `gp2p` command will be available within this image, which can be used to compile graphs and rules. It expects 3 command line arguments:

1. The mode: `graph` or `rule`;
2. The input filepath;
3. The output directory.

A `run` command will be available within this image (for example, for calling within your `Makefile`), which can be used to compile graphs and rules, sending the results for rendering. It expects 4 command line arguments:

1. The mode: `graph` or `rule`;
2. The input filename;
3. The input directory;
4. The output directory.

## Building

In standard Docker fashion:

```bash
$ docker build -t registry.gitlab.com/yorkcs/batman/gp2-printer:latest .
```

And then to publish:

```bash
$ docker push registry.gitlab.com/yorkcs/batman/gp2-printer
```

## Development

The production container image building does not reuse partial compilation results, nor does it show the Haskell files produced by BNFC. We detail how to work around this in development.

Building the parser can be performed by:

```bash
$ cd lib/parser && docker run -v ${PWD}:/data registry.gitlab.com/yorkcs/batman/bnfc-haskell rule && cd ../../
```

Building the project is then:

```bash
$ docker run -i -v ${PWD}:/data haskell:8.6 ghc -i:./data/src -i.:/data/lib/src -i.:/data/lib/parser /data/src/Main.hs
```

Running the project on `test.gp2`:

```bash
$ docker run -i -v ${PWD}:/data haskell:8.6 /data/src/Main graph /data/test.gp2 /data/out
```

FROM registry.gitlab.com/yorkcs/batman/bnfc-haskell:latest
COPY ./lib/parser /parser
WORKDIR /parser
RUN run.sh rule

FROM haskell:8.10-buster
COPY ./ /data
RUN rm -rf /data/lib/parser
COPY --from=0 /parser /data/lib/parser
RUN ghc -i:./data/src -i:./data/lib/src -i:./data/lib/parser /data/src/Main.hs

FROM debian:buster
RUN apt update && apt install -y graphviz libgmp10 make && rm -rf /var/lib/apt/lists/*
COPY --from=1 /data/src/Main /usr/local/bin/gp2p
COPY run.sh /usr/local/bin/
RUN mkdir /data
WORKDIR /data
ENTRYPOINT ["make"]

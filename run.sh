#!/bin/bash

graph() {
    echo Processing $2/$1
    gp2p graph $2/$1 $3 || exit $?
    dot -Tps $3/$1.dot > $3/$1.eps
    echo Written $3/$1.dot.eps
}

rule() {
    echo Processing $2/$1
    gp2p rule $2/$1 $3 || exit $?
    dot -Tps $3/$1-lhs.dot > $3/$1-lhs.eps
    echo Written $3/$1-lhs.dot.eps
    dot -Tps $3/$1-rhs.dot > $3/$1-rhs.eps
    echo Written $3/$1-rhs.dot.eps
}

if [ "$1" == "graph" ]
then
    graph $2 $3 $4
    exit $?
fi

if [ "$1" == "rule" ]
then
    rule $2 $3 $4
    exit $?
fi

echo "ERROR: Unknown mode"
exit 1

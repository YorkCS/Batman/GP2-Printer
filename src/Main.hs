module Main where

import Control.Monad
import Data.Text (splitOn, unpack, pack)
import System.Environment (getArgs)
import System.Exit (exitFailure, exitSuccess, exitWith, ExitCode(ExitFailure))

import ErrM

import LexRule (Token)
import qualified AbsRule
import ParRule (myLexer, pRuleDecl, pGraph)

import Common
import Processor (convertRawRule, convertGraph)
import Presenter (presentRule, presentGraph)


type Lexer = String -> [Token]
type Parser a = [Token] -> Err a
type Processor a b = a -> Err b
type Pipeline a = String -> Err a
type Presenter a = a -> [(String, String)]
type Output = String


rulePipeline :: Lexer -> Parser (AbsRule.RuleDecl a) -> Processor (RawRule a) Rule -> String -> Err Rule
rulePipeline lex par pro str =
  let
    t = lex str
  in case par t of
    Bad err ->
      fail $ unlines ["Parse failed: " ++ err, "Tokens: " ++ show t]
    Ok (AbsRule.RDec _ n a l r i) ->
      pro (RawRule n a l r i Nothing)
    Ok (AbsRule.RDecW _ n a l r i w) ->
      pro (RawRule n a l r i (Just w))


graphPipeline :: Lexer -> Parser (AbsRule.Graph a) -> Processor (AbsRule.Graph a) ([NodeKey], Graph) -> String -> Err Graph
graphPipeline lex par pro str =
  let
    t = lex str
  in case par t of
    Bad err ->
      fail $ unlines ["Parse failed: " ++ err, "Tokens: " ++ show t]
    Ok g ->
      case pro g of
        Ok (_, g) -> return g
        Bad err -> fail err


run :: Pipeline a -> Presenter a -> Output -> String -> IO ()
run pln pnt out str =
  case pln str of
    Bad r -> do
      putStrLn r
      exitWith (ExitFailure 2)
    Ok d -> do
      mapM_ writeF $ pnt d
      exitSuccess
  where
    writeF (path, ctnt) =
      let
        p = out ++ "/" ++ path
      in do
        writeFile p ctnt
        putStrLn $ "Written " ++ p


main :: IO ()
main = do
  args <- getArgs
  case args of
    ["rule", f, o] -> readFile f >>= run (rulePipeline myLexer pRuleDecl convertRawRule) (presentRule $ name f) o
    ["graph", f, o] -> readFile f >>= run (graphPipeline myLexer pGraph (convertGraph [])) (presentGraph $ name f) o
    _ -> do
      putStrLn "Expected a mode followed by exactly one input file and an output directory"
      exitFailure
  where
    name f = unpack $ head $ reverse $ splitOn (pack "/") (pack f)

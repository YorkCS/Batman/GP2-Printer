module Presenter where

import Common
import Printer (renderGraph)
import Data.Maybe (maybeToList)

type Name = String
type Scale = String

presentRule :: Name -> Rule -> [(String, String)]
presentRule f (Rule s l r i w) = 
  let
    scl = scale l r
    l' = f ++ "-lhs"
    r' = f ++ "-rhs"
    tl = [ "\\begin{mdframed}"
         , "\\begin{tabular}{C{0.48\\textwidth}C{0.04\\textwidth}C{0.48\\textwidth}}"
         , "\\multicolumn{3}{l}{\\mintinline{text}{" ++ s ++ "}} \\\\"
         , includeEps scl l' ++ " & \\(\\Rightarrow\\) & " ++ includeEps scl r' ++ " \\\\"
         ]
    tr = [ "\\end{tabular}"
         , "\\end{mdframed}"
         ]
  in
    [ (l' ++ ".dot", renderGraph False i l)
    , (r' ++ ".dot", renderGraph False i r)
    , (f ++ ".tex", unlines (tl ++ maybeToList (fmap present w) ++ tr))
    ]
  where
    present w = "\\multicolumn{3}{l}{\\mintinline{text}{where " ++ w ++ "}} \\\\"
    scale (Graph n0s _) (Graph n1s _) = if length (n0s ++ n1s) > 10 then "0.45" else "0.6"

presentGraph :: Name -> Graph -> [(String, String)]
presentGraph f g =
  let
    scl = scale g
  in
    [ (f ++ ".dot" , renderGraph True 0 g)
    , (f ++ ".tex", "\\fbox{" ++ includeEps scl f ++ "}")
    ]
  where
    scale (Graph ns _) = if length ns > 10 then "0.45" else "0.6"

includeEps :: Scale -> Name -> String
includeEps scale file = "\\includegraphics[scale=" ++ scale ++ "]{" ++ file ++ ".eps}"
